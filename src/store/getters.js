const getters = {
  sidebar: state => state.app.sidebar,
  device: state => state.app.device,
  token: state => state.user.token,
  name: state => state.user.name,
  bind_city: state => state.user.bind_city,
  bind_cid: state => state.user.bind_cid,
  roles: state => state.user.roles,
  permission_routes: state => state.permission.routes,
  second_routes: state => state.permission.second_routes,
  third_routes: state => state.permission.third_routes
}
export default getters
