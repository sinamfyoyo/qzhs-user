import request from '@/utils/request'


export function loginSendCode(data) {
  return request({
    url: 'userAdmin.User/loginSendCode',
    method: 'post',
    data
  })
}

export function login(data) {
  return request({
    url: 'userAdmin.User/userlogin',
    method: 'post',
    data
  })
}

export function getWechatTicket(data) {
  return request({
    url: 'userAdmin.User/getWechatTicket',
    method: 'post',
    data
  })
}

export function getAppoint(data) {
  return request({
    url: 'userAdmin.User/getAppoint',
    method: 'post',
    data
  })
}

export function getQrcodeRandid(data) {
  return request({
    url: 'userAdmin.User/getQrcodeRandid',
    method: 'post',
    data
  })
}

export function getInfo(data) {
  return request({
    url: 'userAdmin.User/getUserInfo',
    method: 'post',
    data
  })
}

export function logout(data) {
  return request({
    url: 'userAdmin.User/logout',
    method: 'post',
    data
  })
}

export function getCity(data) {
  return request({
    url: 'userAdmin.User/getCity',
    method: 'post',
    data
  })
}

export function bindCity(data) {
  return request({
    url: 'userAdmin.User/bindCity',
    method: 'post',
    data
  })
}

export function getmyactionList(data) {
  return request({
    url: 'userAdmin.User/myactionList',
    method: 'post',
    data
  })
}

export function uploadImage() {
  // return process.env.VUE_APP_BASE_API + 'userAdmin.Upload/uploadImage';
  return 'https://wz.mala.cn/userAdmin.Upload/uploadImage';
}
export function uploadFile() {
  // return process.env.VUE_APP_BASE_API + 'userAdmin.Upload/uploadImage';
  return 'https://wz.mala.cn/userAdmin.Upload/uploadFile';
}

export function postVerify(data) {
  return request({
    url: 'userAdmin.User/postVerify',
    method: 'post',
    header: {
      'Content-Type': 'application/x-www-form-urlencoded;'
    },
    data
  })
}
export function getVerifyStatus(data) {
  return request({
    url: 'userAdmin.User/getVerifyStatus',
    method: 'post',
    data
  })
}

export function getSearchUser(data) {
  return request({
    url: 'userAdmin.User/getSearchUser',
    method: 'post',
    data
  })
}

export function getShensu(data) {
  return request({
    url: 'userAdmin.User/getShensu',
    method: 'post',
    data
  })
}
