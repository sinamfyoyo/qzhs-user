import request from '@/utils/request'

export function getCountType() {
  return request({
    url: '/AdminJsonApi/getCountType',
    method: 'get'
  })
}

export function getRankData(data) {
  return request({
    url: '/AdminRankJsonApi/getRankData',
    method: 'post',
    data
  })
}
