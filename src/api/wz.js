
import request from '@/utils/request'

export function getWzParams() {
  return request({
    url: 'userAdmin.Wz/getWzParams',
    method: 'get'
  })
}
export function getWzlist(data) {
  return request({
    url: 'userAdmin.Wz/Wzlist',
    method: 'post',
    data
  })
}


export function getMyWzlist(data) {
  return request({
    url: 'userAdmin.Wz/getMyWzlist',
    method: 'post',
    data
  })
}
export function getWzInfo(data) {
  return request({
    url: 'userAdmin.Wz/getWzInfo',
    method: 'post',
    data
  })
}
export function passonSave(data) {
  return request({
    url: 'userAdmin.Wz/passonSave',
    method: 'post',
    data
  })
}
